# KiCad <=> Allegro interoperability
Scripts for interoperating between KiCad's eeschema and Allegro PCB Designer.

### But...why?
KiCad is a fantastic suite of tools, and development has come a long way.
If pcbnew does everything you need it to do, then please, please use it!
If you run into issues, please [file bugs](https://gitlab.com/kicad/code/kicad/-/issues)
and collaborate with the community in order to help advance pcbnew to greater
heights.

Unfortunately, Allegro PCB Editor is still the industry standard for handling
complex, high-end designs.  It integrates with simulation tools and handles
any bizarre stackup you can come up with.  It is entrenched with ODMs and
specialists in the field.  If you're a PCB designer at a large company, you may
even be required to use the tool, along with a plethora of custom SKILL scripts.

But just because you're stuck with a proprietary back-end tool doesn't mean
you're forced to use a proprietary front-end tool as well.  That's where this
repo comes in: providing a path to using the KiCad suite by starting with
schematic capture.


## allegro\_netlist.py -- Netlisting!
The heart of the project.  An external netlister that converts the interop KiCad
netlist format (.xml) into an Allegro-compatible Telesis netlist and devices
tree.  When you export a netlist, you will get a .txt with the netlist that can
be imported using the Allegro netin "other" tab, and a directory called
"devices" filled with the device files for the relevant parts.  The .txt by
default will be the same name as the root schematic page.

### Interoperability point
The script should work with a variety of library styles, but the main enabling
feature is that your parts should have a footprint filter or footprint field
that has the name of a .psm (without extension) in your Allegro site's psmpath.
Additional entries in the footprint filter list will create a `ALT_SYMBOLS`
property on the part.  Any non-glob filter will be used, so you should be able
to create a hybrid pcbnew/Allegro symbol library -- you'll just have to make
footprints for both backends.

There are a *lot* of additional features that allegro\_netlist.py enables that
diverge from KiCad standard practice, including logical netties and pinswap
definitions.  Read through the source code if you want to take advantage of
everything there is.

### KiCad configuration
The script depends on kicad\_netlist\_reader.py, which is distributed as part of
the KiCad install, or you can download it from the [source tree](https://gitlab.com/kicad/code/kicad/-/blob/HEAD/eeschema/plugins/python_scripts/kicad_netlist_reader.py).
You can either modify PYTHONPATH to include the directory of the file in the
KiCad install, or you can copy the file into the same directory as
allegro\_netlist.py.

In the "Export Netlist" menu, add a generator with a command like so:
```
python3 "/path/to/allegro_netlist.py" "%I" "%O"
```
then use that generator when exporting netlists.  You may need to replace
`python3` with the full path to your python executable if it's not in PATH.

Only python3 is supported for this script.

### Allegro configuration
Make sure the `devpath` variable includes the relative path entry `devices`.


## kicad.il -- Cross-probing!
Enables bidirectional cross-probing between Allegro and eeschema.
The `kicad` Allegro command provided in the file will launch a cross-probe
server as well as standalone eeschema itself.  If the relevant schematic file
cannot be heuristically determined, an open dialog pops up to allow you to
select the KiCad schematic that is associated with the PCB database.

The script adds a KiCad menu that enables easy access to the command.

Cross-probing will not always turn up correct results if your imported netlist
in the PCB is stale compared to your schematic.

Note that net cross-probing is best-effort, even when the designs are in sync.
Some information is lost in the KiCad -> allegro\_netlist.py -> netin -> Allegro
datapath, so net names may not perfectly match up, in which case cross-probing
certain nets may not find anything.  This is especially true if you use logical
netties.

Since eeschema only interacts with a single set of ports, only one instance of
the cross-probe server will run at a time.  Running the `kicad` Allegro command
will stop any other instances, so if you have multiple instances of Allegro
running, the one where you last ran the command will be active for
cross-probing.

### Interoperability point
When eeschema is launched standalone, it listens on a TCP port and attempts to
communicate out on another TCP port in order to cross-probe with a standalone
pcbnew.  This means if you're viewing a schematic that you opened via the KiCad
project manager, you will not be able to crossprobe.  It's easiest to use the
`kicad` command that kicad.il provides in Allegro to launch eeschema to do
cross-probing.

### KiCad configuration
On Linux, `eeschema` should be in PATH (it usually is).  On Windows, `eeschema`
should be the default handler for KiCad schematic files.  This is normally the
case, but you may need to double-click on a schematic file to ensure the default
handler is set.

### Allegro configuration
Load kicad.il in the relevant .ilinit like you would any other SKILL script.
xprobe\_server.py must be in the same directory as kicad.il.

By default, the `python3` command is used to launch the server.  On Linux, this
is probably in your PATH and everything is hunky-dory.  On Windows, you can
either install python3 (standalone or from the Windows store) or use the
python2.7 install hidden deep in the Allegro install tree (if it's still there).
If you're using python3, you can either add the executable directory to PATH, or
modify the `python` variable in Allegro to point to the full path of the python3
executable.  If you're using python2.7, you must set the `python` variable to
point to the executable.

python2.7 is supported by the cross-prober script as a convenience in the
situation where the PCB designer is on a locked-down Windows configuration where
installing python3 may be inconvenient or impossible.  It allows you to
distribute the cross-probing scripts as part of your Allegro site, with no
external dependencies (other than KiCad itself).

Pro-tip: this also works with Allegro Viewer Plus, so if you're just inspecting a
design, you can save your Designer licenses and use Viewer licenses instead.
This does *not* work with the free viewer, as that tool doesn't support SKILL.


## xprobe\_server.py
Python server used by kicad.il in Allegro to service communication with eeschema
for cross-probing.  Compatible with both python3 and python2.7.  There's no
reason to manually run it unless you're debugging the implementation, in which
case set `DEBUG` to `True` in the file and (optionally) launch it from the
command line to see all the communication going in and out.  If you launch it
from the command line, `stdin` are the requests from Allegro in the format that
kicad.il generates, and `stdout` are the SKILL commands from the server for
Allegro to execute.
