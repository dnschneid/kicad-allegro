#!/usr/bin/env python3
# SPDX-FileCopyrightText: 2024 Rivos Inc.
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
# SPDX-License-Identifier: Apache-2.0

# This script works with both python2.7 and python3.x

import os
import socket
import subprocess
import sys
from threading import Thread
import time
try:
  from socketserver import BaseRequestHandler, TCPServer
except ImportError:
  from SocketServer import BaseRequestHandler, TCPServer

DEBUG = False

EESCHEMA_HOST = 'localhost'
EESCHEMA_RECV = 4242
EESCHEMA_SEND = 4243

NET_EXT = '.txt'
SCH_EXT = '.kicad_sch'

def log(txt):
  if DEBUG:
    sys.stderr.write(txt + '\n')
    sys.stderr.flush()

def eeschema(cmd):
  log(cmd)
  sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  if sock.connect_ex((EESCHEMA_HOST, EESCHEMA_SEND)) == 0:
    sock.sendall(cmd.encode())
  sock.close()

def skill(cmd):
  """ Executes a skill command. """
  log(cmd)
  sys.stdout.write(cmd + '\n')
  sys.stdout.flush()

def skill_highlight_and_zoom(targets):
  return skill('let(((targ {}))'
               '    axlHighlightObject(targ)'
               '    axlZoomToDbid(targ t))'.format(targets))


class KiCadReceiver(BaseRequestHandler):
  # See kicad/pcbnew/cross-probing.cpp for command reference
  prev_targets = None

  @staticmethod
  def start():
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    if sock.connect_ex((EESCHEMA_HOST, EESCHEMA_RECV)) == 0:
      log('Taking over cross-probing from another instance')
      sock.sendall('$QUIT'.encode())
    sock.close()
    for _ in range(10):
      try:
        return TCPServer((EESCHEMA_HOST, EESCHEMA_RECV), KiCadReceiver)
      except socket.error:
        time.sleep(0.1)
    return None

  def handle(self):
    cmd, _, data = self.request.recv(4096).decode().partition(' ')
    # Clear existing highlight before starting the next one
    if KiCadReceiver.prev_targets:
      skill('axlDehighlightObject({})'.format(KiCadReceiver.prev_targets))
      KiCadReceiver.prev_targets = None
    if cmd == '$SELECT:':
      # $SELECT: 0,FJ505,FJ506,FJ507,FQ503
      targets = data.split(',')
      action = targets.pop(0)  # 0: select target, 1: select target+connections
      # S<Sheet path>, F<Reference>, P<Footprint reference>/<Pad number>
      # Support highlighting refdes and pins
      targets = tuple(t[1:] for t in targets if t[:1] in 'FP' and t[1:2] != '#')
      if targets:
        # Generate an Allegro dbid list across all the selected refdes.
        # Include post-replication refdes that may be in the board as well.
        # Filter out the ones that could not be found (nil entries).
        # By using map and axlDBFindByName, we avoid using axlSelectByName
        # which will spit out errors to the console when a tool is active.
        targets = (
            # setof: go through the resulting list and drop nil entries
            "setof(id mapcan("  # mapcan: concatenate returned lists from lambda
            "    lambda((r)"  # per refdes, start a list of objects
            "      r = parseString(r \"/\")"  # separate refdes from pin number
            "      let(((p list(axlDBFindByName('refdes car(r)))) (i 1))"
            "        while(or(i==1 car(p))"  # collect replicate refs until nil
            "          pushf("  # in-place prepend of p
            "            axlDBFindByName('refdes"
            "              sprintf(nil \"%s_%d\" car(r) i++))"
            "            p))"
            "        if(i=cadr(r)"  # handle pin selections by finding the pin
            "          mapinto(p lambda((o)"  # update object list in place
            "              car(exists(x o->pins x->number==i)))"  # find pin
            "            p))"
            "        p)"  # return the list
            "    ) '(\"{}\")"  # list of refdes from kicad goes here
            "  ) id)"  # only keep ids that are not nil
            .format('" "'.join(targets)))
        skill_highlight_and_zoom(targets)
        KiCadReceiver.prev_targets = targets
    elif cmd == '$NET:':
      # $NET:  "netname"
      targets = 'axlDBFindByName(\'net {})'.format(data)
      skill_highlight_and_zoom(targets)
      KiCadReceiver.prev_targets = targets
    elif cmd == '$CLEAR':
      # Already cleared
      pass
    elif cmd == '$QUIT':
      log("another cross-probe server is taking over")
      Thread(target=self.server.shutdown).start()


class AllegroReceiver(Thread):
  # See kicad/eeschema/cross-probing.cpp for command reference
  def __init__(self, oneol=None):
    Thread.__init__(self)
    self.daemon = True
    self._oneol = oneol
  def run(self):
    last_cmd_time = 0
    for cmd in iter(sys.stdin.readline, ''):
      # Rate-limit to avoid highlight spam on multi-select
      cur_cmd_time = time.time()
      if 0 <= cur_cmd_time - last_cmd_time < 0.1:
        continue
      last_cmd_time = cur_cmd_time
      cmd = cmd.strip().partition(':')
      if cmd[0] == 'symbol':
        eeschema('$PART: "{}"'.format(cmd[2].partition('_')[0]))
      elif cmd[0] == 'pin':
        ref = cmd[2].partition('.')
        eeschema('$PART: "{}" $PAD: "{}"'.format(
          ref[0].partition('_')[0], ref[2]))
      elif cmd[0] == 'net':
        # TODO: fuzzy-match net name with the xml netlist, if available
        eeschema('$NET: "{}"'.format(cmd[2].upper()))
      elif cmd[0] == 'schematic':
        launch_eeschema(cmd[2])
    if self._oneol:
      self._oneol()


def find_schematic(brdpath):
  """ Heuristically finds likely schematic for a brd file """
  brddir = os.path.dirname(brdpath)
  txts = []
  # Look at the design directory and one directory up.
  for path in (brddir or '.', os.path.join(brddir, '..')):
    files = os.listdir(path)
    # Find txt (netlist) files with matching kicad_sch files
    txts.extend(os.path.join(path, f) for f in files
        if f.endswith(NET_EXT) and f.replace(NET_EXT, SCH_EXT) in files)
  if not txts:
    log('No schematics with netlists found nearby')
    return None
  for txt in txts:
    sch = txt.replace(NET_EXT, SCH_EXT)
    # See if the txt file name shows up in the brd file
    txt = os.path.basename(txt).encode() + b'\0'
    for data in open(brdpath, 'rb'):
      if txt in data:
        return sch
  log('None of the netlists were referenced by the brd file')
  return None


def launch_eeschema(path):
  if not os.path.isfile(path):
    return False
  if path.endswith(SCH_EXT):
    sch = path
  else:
    sch = find_schematic(path)
    if not sch:
      # Couldn't find. Launch the picker, which will send a new command.
      skill('let(((p axlDMFileBrowse(nil nil ?noDirectoryButton t ?noSticky t'
            ' ?title "Select the root schematic file"'
            ' ?optFilters "KiCad schematics (*{})|*{}")))'
            '   if(p kicad(p)))'.format(SCH_EXT, SCH_EXT))
      return False
  log('Launching eechema for {}'.format(sch))
  if hasattr(os, 'startfile'):
    os.startfile(os.path.normpath(sch))
  else:
    DEVNULL = open(os.devnull, 'w')
    subprocess.Popen(['eeschema', sch],
        stdin=DEVNULL, stdout=DEVNULL, stderr=DEVNULL, close_fds=True)
  return True


if __name__ == "__main__":
  serv = KiCadReceiver.start()
  if not serv:
    log('Unable to start server')
    sys.exit(1)
  log('Server started')
  AllegroReceiver(serv.shutdown).start()
  serv.serve_forever()
